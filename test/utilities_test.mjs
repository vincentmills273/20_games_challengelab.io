import { expect } from "chai";
import { sortTables } from "../assets/js/utility.mjs";
import { JSDOM } from "jsdom";

const simulateClickOn = (target) => {
  const evt = document.createEvent("HTMLEvents");
  evt.initEvent("click", false, true);
  document.querySelector(target).dispatchEvent(evt);
};

const getValuesFromColumn = (colIndex) => {
  return [...document.querySelector("table tbody").children].map(
    (row) => row.children[colIndex].textContent
  );
};

describe("sortTables", function () {
  beforeEach(function () {
    return JSDOM.fromFile("test/testTables.html").then((dom) => {
      global.window = dom.window;
      global.document = window.document;
    });
  });
  describe("sorts alphabetically", () => {
    it("ascending", function () {
      sortTables();
      simulateClickOn("thead > tr > th:nth-child(1)");

      const firstColumnValues = getValuesFromColumn(0);
      expect(firstColumnValues).to.eql(["A", "B", "C"]);
    });
    it("descending", function () {
      sortTables();
      simulateClickOn("thead > tr > th:nth-child(1)");
      simulateClickOn("thead > tr > th:nth-child(1)");
      const firstColumnValues = getValuesFromColumn(0);
      expect(firstColumnValues).to.eql(["C", "B", "A"]);
    });
  });

  describe("sorts numerically", () => {
    it("ascending", () => {
      sortTables();
      simulateClickOn("thead > tr > th:nth-child(2)");
      const secondColumnValues = getValuesFromColumn(1);
      expect(secondColumnValues).to.eql(["0.5", "0.5", "1.0"]);
    });
    it("when clicked twice, descending", () => {
      sortTables();
      simulateClickOn("thead > tr > th:nth-child(2)");
      simulateClickOn("thead > tr > th:nth-child(2)");
      const secondColumnValues = getValuesFromColumn(1);
      expect(secondColumnValues).to.eql(["1.0", "0.5", "0.5"]);
    });
    it("when clicked three times, ascending", () => {
      sortTables();
      simulateClickOn("thead > tr > th:nth-child(2)");
      simulateClickOn("thead > tr > th:nth-child(2)");
      simulateClickOn("thead > tr > th:nth-child(2)");
      const secondColumnValues = getValuesFromColumn(1);
      expect(secondColumnValues).to.eql(["0.5", "0.5", "1.0"]);
    });
  });
});
