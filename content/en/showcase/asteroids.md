---
title: "Asteroids Showcase"
anchor: "asteroids_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}


* Eвгений made a [web playable game](https://kaoruyamazaki.itch.io/asteroids) with [source code](https://github.com/evgenydrainov/20GamesChallenge)

* Erip made a [web playable game](https://games.petzel.io/asteroids/index.html)

* Kibble made a [web playable game](https://dogfoodnight.itch.io/mouseteroids)

* Local Man made a [web playable game](https://local-mans.itch.io/asteroids)

* Secondhand Gnome made a [web playable game](https://secondhandgnome.itch.io/cashteroids)

* Crazillo made a [web playable game](https://homefrog-games.itch.io/asteroids-20-games-challenge)

* Tom is Green recorded some gameplay:
  {{< youtube cNAKHK4_l8Q >}}

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/asteroids):
  {{< youtube zX4clPDU9cA >}}

{{< include file="_parts/showcase_footer.md" type=page >}}