---
title: "Super Mario Bros Showcase"
anchor: "mario_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Erip made a [web playable game](https://games.petzel.io/mario/)

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/jump-man):
  {{< youtube uzu6PchSxoY >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
