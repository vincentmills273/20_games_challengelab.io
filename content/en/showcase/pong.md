---
title: "Pong Showcase"
anchor: "pong"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Woofenator made a [web playable game](https://woofenator.itch.io/g1-clong) with [source code](https://github.com/Woofenator/pong) and a [devlog](https://foulleaf.dev/gamedev/2023/03/03/game-1-clong.html)

* theMinesAreShakin made a [web playable game](https://theminesareshakin.itch.io/ping) with a [devlog](https://jakemeinershagen.com/ping) and [source code](https://github.com/jakemeinershagen/ping)

* Nkr made two pong clones with source code, [one in C](https://github.com/paezao/pong) and [one in JS](https://github.com/paezao/pong-js)

* BeepsNBoops made a [web playable game](https://dumdumman.itch.io/small-pong) with [source code](https://github.com/PBnJK/small-pong)

* Andrew made a [web playable game](https://bright-licorice-f91cc3.netlify.app/) with [source code](https://gitlab.com/frenata-20-game-challenge/pong)

* zyx750 made a [downloadable game](https://zyx750.itch.io/pong) with [source code](https://github.com/Zyx750/Pong)

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/plingplong)

* SereneJellyfish made a [web playable game](https://serenejellyfishgames.itch.io/paddle-wars)

* Xithas made a [web playable game](https://xithas.itch.io/the-neon-pong-game)

* Urahara9364 made a [web playable game](https://urahara9364.itch.io/piring-porong)

* xxsoranogenkai made a [web playable game](https://play.unity.com/mg/other/webgl-builds-296495)

* Magikmw made a [web playable game](https://michalwalczak.eu/pong/pong.html)

* Jimothy made a [web playable game](https://gx.games/games/gb97pa/orange-pong/)

* JJBandana made a game and shared [source code](https://github.com/JJBandana/pong)

* Dallai made a game and shared [source code](https://github.com/Dallai-Studios/Super-Pong/releases/tag/1.0.0)

* Erip made a [web playable game](https://games.petzel.io/pong/index.html)

* BanMedo made a [web playable game](https://banmedo.itch.io/chill-pong)

* Kibble made a [web playable game](https://dogfoodnight.itch.io/pong-clone)

* Crazillo made a [web playable game](https://homefrog-games.itch.io/pong-20-games-challenge)

* Nate made [downloadable game](https://buzjr.itch.io/pong) and a video devlog:
  {{< youtube xmjVZxuX1As >}}

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/pong):
  {{< youtube Ol5oDW5Ccns >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
