---
title: "Frogger Showcase"
anchor: "frogger_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Erip made a [web playable game](https://games.petzel.io/frogger/index.html)

* Kibble made a [web playable game](https://dogfoodnight.itch.io/frogger-clone)

* Crazillo made a [web playable game](https://homefrog-games.itch.io/the-20-games-challenge)

* Primal Dialga made a [web playable game](https://tarnest.itch.io/frogger-clone)

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/frogger):
  {{< youtube XO3JjkDpd34 >}}

* More to come!

{{< include file="_parts/showcase_footer.md" type=page >}}
