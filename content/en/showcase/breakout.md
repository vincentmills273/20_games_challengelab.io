---
title: "Breakout Showcase"
anchor: "breakout_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Arakeen made a [web playable game](https://ctn-phaco.itch.io/simple-breakout) with [source code](https://github.com/AlixBarreaux/simple-breakout)

* SereneJellyfish made a [web playable game](https://serenejellyfishgames.itch.io/beachside-breakout)

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/generic-breakout-clone)

* Martal made a [web playable game](https://martialis39.itch.io/brick-game)

* NotSoComfortableCouch made a [web playable game](https://notsocomfortablecouch.itch.io/breakwave)

* Jackson made a [web playable game](https://districtjackson.itch.io/break-the-breakout)

* Erip made a [web playable game](https://games.petzel.io/breakout/index.html)

* Woofenator made a [web playable game](https://woofenator.itch.io/break-in)

* Kibble made a [web playable game](https://dogfoodnight.itch.io/breakout-clone)

* Walid made a [web playable game](https://waliddib.itch.io/breakout)

* CheopisIV made a [downloadable game](https://cheopisiv.itch.io/breaker-2023)

* Cat/Jess made a [downloadable game](https://solipsisdev.itch.io/brick-break)

* Crazillo made a [web playable game](https://homefrog-games.itch.io/breakout-20-games-challenge)

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/breakout):
  {{< youtube xbw1DKH0pqA >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
