---
title: "Space Invaders Showcase"
anchor: "invaders_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Andrew made a [web playable game](https://frenata.itch.io/space-invaders) with [source code](https://gitlab.com/frenata-20-game-challenge/space-invaders)

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/the-day-the-eldritch-came)

* Local Man made a [web playable game](https://local-mans.itch.io/invaders)

* T$ made a [web playable game](https://tmichal2.itch.io/space-invaders)

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/space-invaders):
  {{< youtube kV5N86ULkpw >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
