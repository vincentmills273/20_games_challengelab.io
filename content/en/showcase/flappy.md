---
title: "Flappy Bird Showcase"
anchor: "flappy_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Niashi made a [web playable game](https://niashi24.github.io/Projects/FlappyBird/index.html) with [source code](https://github.com/Niashi24/FlappyClone)

* Arakeen made a [web playable game](https://ctn-phaco.itch.io/flappy-hat) with [source code](https://github.com/AlixBarreaux/flappy-hat)

* Valumin made a [web playable game](https://valumin.itch.io/flappy-ufo)

* txorimalo made a [web playable game](https://txorimalo.itch.io/helloucar)

* CakeBlood made a [web playable game](https://cakeblood.itch.io/flappy-gunnar)

* Bumpi made a [web playable game](https://bumpi.itch.io/another-flappy-bird-clone)

* NotSoComfortableCouch made a [web playable game](https://notsocomfortablecouch.itch.io/flappy-thingy)

* Jimothy made a [web playable game](https://gx.games/games/dyizk2/flappy-eagle/)

* Nazorus made a [web playable game](https://nazorus.itch.io/jetcave)

* AllHeart made a [web playable game](https://gx.games/games/busvgk/flying-fish/tracks/c38f865d-9823-4744-8f00-95ff8f8737a6)

* Erip  made a [web playable game](https://games.petzel.io/flappybird/index.html)

* Juan Carlos made a a [web playable game](https://1juancarlos.itch.io/flappy-bird-vanilla)

* Karmanya  made a a [web playable game](https://karmanya007.itch.io/elevate-a-winged-journey)

* BanMedo  made a a [web playable game](https://banmedo.itch.io/fly-er)

* Boofgall made a a [web playable game](https://boofgall-games.itch.io/ballon-survival)

* Tom is Green made a a [downloadable game](https://tomisgreen.itch.io/cyberflap-2084)

{{< include file="_parts/showcase_footer.md" type=page >}}
