---
title: "Indy 500 Showcase"
anchor: "indy_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Greg made a [web playable game](https://an-unque-name.itch.io/indy500atari-x-pirates)

* Ringo Skulkin made a [downloadable game](https://krish-garg.itch.io/indy-500-clone)

{{< include file="_parts/showcase_footer.md" type=page >}}
