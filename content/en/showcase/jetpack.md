---
title: "Jetpack Joyride Showcase"
anchor: "jetpack_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Andrew made a [web playable game](https://tourmaline-trifle-efc59f.netlify.app) with [source code](https://gitlab.com/frenata-20-game-challenge/jetpack)

* Jimothy made a [web playable game](https://gx.games/games/8vccin/hoverboard-joyride/)

* Juan Carlos made a [web playable game](https://1juancarlos.itch.io/jumbas-joyride)

* CakeBlood made a [web playable game](https://cakeblood.itch.io/sleigh-rider)

* Nate made a [downloadable game](https://buzjr.itch.io/jetpack-joyride) and a video devlog:
  {{< youtube bIySGIJCALk >}}

* Luke Muscat (the original designer for Jetpack Joyride) made an interesting video about the game:<br>(The video wasn't for this challenge, I just thought it was an interesting resource to link)
  {{< youtube mxHkXADm3gU >}} 

{{< include file="_parts/showcase_footer.md" type=page >}}
