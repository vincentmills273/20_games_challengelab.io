---
title: "List of Games"
anchor: "games"
geekdocCollapseSection: true
weight: 40
---

Below, you will find a list of every game that I thought to add to the challenge. If you think that a game is missing, feel free to check the [contributing]({{< ref "contributing/_index.md" >}}) page to get it added!  

**I'm still adding details for the majority of these games** 

Each game in the list below will have a short description, as well as a Definition of Done and a difficulty estimate. Click on the game name to see its details.

{{< expand "See an example">}} {{< include file="_parts/example.md" type=page >}} {{< /expand >}}

{{< hint type=important title="This page is still in progress" >}}
I'm currently trying to sort and evaluate the games below. Each one will get its own page, with a more detailed and accurate estimate of complexity and scope. For now, everything on this page is a best guess.

The [challenge page]({{< ref "challenge/_index.md" >}}) is a good place to start in the meantime.
{{< /hint >}}

{{< css >}}

# Game List:
| **Game** {{< svgArrow >}} | **Complexity** {{< svgArrow >}} | **Scope** {{< svgArrow >}} | **Platform** {{< svgArrow >}} | **Release Year** {{< svgArrow >}} |
| :---                                                    | :--- | :--  | :---              | :--- |
| [Pong]({{< ref "games/pong.md">}})                      | 0.5  | 0.5  | Arcade, Console   | 1972 |
| [Flappy Bird]({{< ref "games/flappy.md">}})             | 1.0  | 1.0  | Mobile            | 2013 |
| [Breakout]({{< ref "games/breakout.md">}})              | 0.5  | 0.5  | Arcade, Console   | 1976 |
| [Jetpack Joyride]({{< ref "games/jetpack.md">}})        | 1.0  | 1.5  | Mobile            | 2011 |
| [Space Invaders]({{< ref "games/invaders.md">}})        | 1.5  | 2.0  | Arcade            | 1978 |
| [Frogger]({{< ref "games/frogger.md">}})                | 1.5  | 2.0  | Arcade            | 1981 |
| [River Raid]({{< ref "games/river_raid.md">}})          | 2.0  | 2.0  | Console           | 1982 |
| [Asteroids]({{< ref "games/asteroids.md">}})            | 1.5  | 1.0  | Arcade            | 1979 |
| [Spacewar!]({{< ref "games/spacewar.md">}})             | 1.5  | 1.0  | Arcade            | 1979 |
| [Indy 500]({{< ref "games/indy.md">}})                  | 2.0  | 1.0  | Arcade            | 1979 |
| [Lunar Lander]({{< ref "games/lander.md">}})            | 2.0  | 1.0  | Arcade            | 1979 |
| [Pac-Man]({{< ref "games/pacman.md">}})                 | 2.0  | 1.5  | Arcade            | 1980 |
| [Tic-Tac-Toe]({{< ref "games/tic_tac_toe.md">}})        | 2.0  | 1.5  | Hardware          | 1950 |
| [Conway's Game of Life]({{< ref "games/life.md">}})     | 2.0  | 1.5  | Various           | 1970 |
| [Mario Bros]({{< ref "games/mario.md">}})               | 2.5  | 3.0  | Arcade            | 1985 |
| [Pitfall]({{< ref "games/pitfall.md">}})                | 2.5  | 3.0  | Arcade            | 1982 |
| [VVVVVV]({{< ref "games/vvvvvv.md">}})                  | 2.0  | 2.0  | Computer          | 2010 |
| [Worms]({{< ref "games/worms.md">}})                    | 3.0  | 3.0  | Computer          | 1995 |
| [Dig Dug]({{< ref "games/dig_dug.md">}})                | 2.5  | 2.0  | Arcade            | 1982 |
| [(Super) Motherload]({{< ref "games/motherload.md">}})  | 2.5  | 2.5  | Web (Flash)       | 2013 |
| [Super Monkey Ball]({{< ref "games/monkeyball.md">}})   | 3.5  | 3.0  | Console           | 2001 |
| [Star Fox]({{< ref "games/star_fox.md">}})              | 3.5  | 4.0  | Console           | 1993 |
| [Crash Bandicoot]({{< ref "games/crash.md">}})          | 3.5  | 3.5  | Console           | 1996 |
| [Doom]({{< ref "games/doom.md">}})                      | 3.5  | 3.5  | Computer          | 1993 |
| [Mario Kart]({{< ref "games/mario_kart.md">}})          | 3.5  | 3.5  | Console           | 1992 |
| [Minecraft]({{< ref "games/minecraft.md">}})            | 4.0  | 2.0  | Computer          | 2009 |
| [Portal]({{< ref "games/portal.md">}})                  | 4.0  | 3.0  | Computer          | 2007 |
| Chrome Dinosaur Game                                    | 0.5  | 0.5  | Web               | 1986 |
| Moon Patrol                                             | 1.5  | 2.0  | Arcade            | 1982 |
| Blokade (Snake)                                         | 1.0  | 0.5  | Arcade            | 1976 |
| Tetris                                                  | 2.0  | 1.0  | Console, Handheld | 1984 |
| Snail Maze                                              | 1.0  | 1.0  | Console           | 1986 |
| Missile Command                                         | 1.5  | 1.5  | Arcade            | 1980 |
| Doodle Jump                                             | 1.5  | 1.0  | Mobile            | 2009 |
| Donkey Kong                                             | 2.0  | 1.5  | Arcade            | 1981 |
| The Legend of Zelda                                     | 2.0  | 3.0  | Console           | 1986 |
| Cookie Clicker                                          | 1.0  | 1.0  | Computer          | 2013 |
| Zork                                                    | 2.0  | 1.0  | Computer          | 1980 |
| Minesweeper                                             | 1.5  | 1.0  | Computer          | 1989 |
| Bejeweled                                               | 1.5  | 1.0  | Computer          | 2001 |
| Guitar Hero                                             | 1.5  | 1.5  | Console           | 2005 |
| Super Mario Bros                                        | 2.0  | 2.0  | Console           | 1985 |
| Peggle                                                  | 2.0  | 2.0  | Computer          | 2007 |
| Rogue                                                   | 2.5  | 1.0  | Computer          | 1980 |
| Sonic the Hedgehog                                      | 2.5  | 3.0  | Console           | 1991 |
| Line Rider                                              | 3.0  | 1.0  | Computer          | 2006 |
| Hill Climb Racing                                       | 2.5  | 2.5  | Mobile            | 2012 |
| Marble Madness                                          | 3.0  | 2.0  | Arcade            | 1984 |
| Rampage                                                 | 2.5  | 3.0  | Arcade            | 1986 |
| Lemmings                                                | 3.0  | 3.0  | Computer          | 1991 |
| Prince of Persia                                        | 3.5  | 3.5  | Computer          | 1989 |
| Pokémon                                                 | 3.5  | 3.0  | Handheld          | 1996 |
| Full Tilt Pinball                                       | 2.5  | 2.5  | Computer          | 1995 |
| Tiny Wings                                              | 2.5  | 2.0  | Mobile            | 2011 |
| Secret of Monkey Island                                 | 4.0  | 4.0  | Console           | 1990 |
| Double Dragon                                           | 3.5  | 4.5  | Arcade            | 1987 |
| Zaxxon                                                  | 3.0  | 3.0  | Arcade            | 1981 |
| Maze War                                                | 2.0  | 1.0  | Computer          | 1974 |
| Wipeout (F0)                                            | 4.0  | 3.0  | Console           | 1995 |
| Diablo                                                  | 3.5  | 5.0  | Computer          | 1997 |
| Quake                                                   | 4.0  | 3.0  | Computer          | 1996 |
| Super Mario 64                                          | 4.5  | 4.0  | Console           | 1996 |
| Rocket League                                           | 4.5  | 2.5  | Console, PC       | 2015 |
| Gran Turismo                                            | 5.0  | 4.0  | Console           | 1997 |
| FTL: Faster Than Light                                  | 4.5  | 5.0  | Computer          | 2012 |
| Roller Coaster Tycoon                                   | 5.0  | 5.0  | Computer          | 1999 |
| Star Craft                                              | 5.0  | 5.0  | Computer          | 1998 |
| Dune II                                                 | 5.0  | 5.0  | Computer          | 1992 |
| Sim City                                                | 5.0  | 3.5  | Computer          | 1989 |
| The Sims                                                | 5.0  | 5.0  | Computer          | 2000 |
| Command & Conquer                                       | 5.0  | 5.0  | Computer          | 1995 |

{{< SortTable >}}

