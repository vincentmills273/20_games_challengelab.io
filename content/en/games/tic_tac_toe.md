---
title: "Tic Tac Toe"
anchor: "tic_tac_toe"
weight: 51
---

Tic-tac-toe (also known as noughts and crosses) is a fairly simple game played on a grid. Because of its low complexity, this game was one of the earliest to be solved by computers. In 1950 (two decades before Pong), an "arcade game" named Bertie the Brain was shown off at the Canadian National Exhibition. Bertie would play tic-tac-toe against human opponents using a bank of colored light bulbs. Two years later, the EDSAC research computer was programmed to play tic-tac-toe on a CRT monitor. Both computers were able to play optimally against a human player.

| ***Difficulty*** |                                              |
| :---             | :---                                         |
| Complexity       | {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}}                          | 

### Goal:
* Create a 3 by 3 grid for the game board. Each cell in the grid should be able to hold an X, an O, or be empty.
* Make each cell selectable. Two players should alternate turns. One player is X, and the other is O. When an empty cell is clicked, an X or O should be placed there.
* Detect a win condition. If three X's or three O's are in a row (vertically, horizontally, or diagonally), then the player who placed them wins. If the board is filled before someone gets three in a row, then the game is a tie.
* Create an AI opponent. The AI needs to be able to read the board state, evaluate the best move to make, and then place an X or O in the appropriate cell. Try to make an AI yourself before looking up solutions online. Your ideas probably won't be as good as the collective internet, but you'll learn more if you think if through for yourself first.
{{< hint type=tip title="Don't forget to show your work off!">}}
AI design isn't just about making the best algorithm. Good AI systems will show off their intelligence to the player in order to feel more human. Try adding an artificial delay on the AI's turn to make it feel like it's thinking for a second or two.

You could take it a step farther and add a chat window for the computer player to tell you what it's thinking. (This kind of communication is called "barks.") If you can find a way to show your work off to the player, they will see more of the complexity in your AI (even if it is faked), and think that it is "smarter" as a result.
{{< /hint >}}
* Add menus and a UI. Consider adding a match counter for multiple rounds.

### Stretch goal:
* Once you have your AI working, look up optimal strategy tables to see if your AI is playing as well as it can. Play code golf - try to refactor the AI to be as small, simple, or fast as possible.
* You can also try implementing an online AI at this point (like the minimax algorithm). You can even set up a 0 player game to let your first AI play against the internet's AI. Experiment with game trees, 

{{< expand "Showcase" >}} {{< include file="showcase/tic_tac_toe.md" type=page >}} {{< /expand >}}
