---
title: "Mario Kart"
anchor: "text"
weight: 91
---

In 1992, Nintendo released it's first kart racing game, Super Mario Kart. The game started out as a generic kart racing game, but the team decided to test out the game with Mario and friends behind the wheel. It was a perfect match, and the theme stuck. The original Super Mario Kart used "Mode-7 Graphics" to simulate 3D. The second game, Mario Kart 64, featured true polygonal terrain with billboard sprites for the players. The Mario Kart series has undergone a gradual refinement; the most recent game features many of the themes, tracks, and items that were introduced in the very first game.

Feel free to re-make your favorite map or item, or add your own flavor to the game!

| ***Difficulty*** |                                                                                      |
| :---             | :---                                                                                 |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} | 

### Goal:
* Implement a kart for the player to drive. The kart should be able to accelerate, brake, and turn.
* Create one or more tracks. The track should be a complete circuit with a start/finish line. Add hills, jumps or other special features.
* Add AI opponents. They should be able to race around the track alongside the player. Mario Kart heavily features "Rubber-banding," so you should add that too.
{{< hint type=tip title="Why Rubber Banding?" >}}
Rubber-Banding is a controversial topic in video games. Players want to be challenged, but they don't want to feel like they are being played down to.

Mario Kart will empower players in the back and disempower players in the front. Some games in the series were more overt than others. Try to find a balance in your version; make the game fun and exciting without being overtly condescending.
{{< /hint >}}
* Add item boxes to the track. Add a few different types of items (mushroom, shell, banana, etc.), and allow both the player and the AI racers to use the items.
* Add a UI with lap counter, lap time, and position.

### Stretch goal:
* Add a "hop and drift to boost" mechanic. Add a button that allows the kart to jump. After landing, the kart will be able to drift around corners (the facing direction is no longer in-line with the direction of travel)
* Implement a mini-map that shows the position of the player and other racers.
* Try your hand at networking, and make the game multiplayer!

{{< expand "Showcase" >}} {{< include file="showcase/mario_kart.md" type=page >}} {{< /expand >}}
