---
title: "Pitfall"
anchor: "pitfall"
weight: 61
---

According to its designer, Pitfall took 10 minutes to design, but 1000 hours to program. The game released in 1982, and was the #1 best selling Atari 2600 game for more than a year. The Atari 2600 could only draw two player sprites and three one-pixel bullets (it was essentially designed to play Pong), so Pitfall had to move or modify the sprites on every scanline in order to build the complex world.

The entire world was built from an 8 bit polynomial counter, resulting in 255 pseudorandom playable screens. The entire game took only 4 kilobytes of ROM! The goal of the game is to collect 32 treasures in the world within 20 minutes.

| ***Difficulty*** |                                              |
| :---             | :---                                         |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}}      | 

### Goal:
* Create a 2D character controller. The player should be able to walk left or right, jump, hold onto swinging vines, and climb up and down ladders.
* Create a world made up of pseudo-randomly generated game screens. Each screen should contain an upper level and an underground tunnel. The upper level should have one of the following:
  * A ladder connecting the upper and lower levels, or a ladder with a hole on each side that the player can fall through.
  * A tar pit (black). The pit can be crossed by swinging on a vine, or will expand and contract, allowing the player to run across.
  * A lake (blue). The lake can by crossed by jumping across the heads of 3 alligators that will open and close their mouths. Alternatively, the lake can expand and contract, or be crossed with a vine.
  * There are also logs that will roll across the screen on some levels, causing the player to trip.
  * There can be a static hazard to jump over (a fire or a snake), or a treasure to collect.
* The tunnel will contain a scorpion that walks towards the player. Alternatively, it can contain a ladder and/or a brick wall. The brick wall blocks the path, forcing the player to go above-ground.
* Exiting a screen above-ground will move to the next (or previous screen). Exiting a screen in the tunnel will move forward or backward 3 screens.
* Make sure to record your best Tarzan yell for the player when he swings on a vine. (You should probably add other sound effects, too.)
* Add some background art as well. Moving the trees around for each level will give the impression that you are wandering through a random forest instead of just visiting the same screen repeatedly.
* Add a score counter, life counter, and timer.

### Stretch goal:
* The original game only took 4 KB of disk space. You won't get close, but do try to optimize you game a bit. See if you can figure out how to derive an entire map from an 8-bit number. (If you can't figure it out, feel free to look it up, it's a really cool piece of code!)
* Feel free to make a "modern remake" of Pitfall with fancier graphics and sprite animations.

{{< expand "Showcase" >}} {{< include file="showcase/empty.md" type=page >}} {{< /expand >}}
