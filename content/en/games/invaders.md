---
title: "Space Invaders"
anchor: "invaders"
weight: 30
---

The launch of Space Invaders in 1978 marked the beginning of the Golden Age of the Arcade. Space Invaders was one of the first games to use a microcomputer. However, off-the-shelf technology was not available, so designer Tomohiro Nishikado had to design the computer hardware himself and write his own development tools before he could program the game. The hardware still struggled to render all of the sprites, so the game (and music) would speed up as aliens were destroyed. Modern systems won't lag for a game this simple, so you might have to add that feature deliberately!

| ***Difficulty*** |                                         |
| :---             | :---                                    |
| Complexity       | {{< icon "star" >}}                     |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} | 

### Goal:
* Create a player ship that moves side to side.
* Create a few different types of alien invaders.
  * Enemies will move together in a grid. They cross the screen horizontally before dropping vertically and reversing their direction.
* Add the ability for the player ship to fire rockets that travel up the screen.
* Add bombs/bullets that the enemies drop. The player's rockets can destroy enemy bullets.
* Make sure that the player's bullets will destroy invaders, and the invader bullets will destroy the player.
* Add a mothership that will cross the screen periodically. Destroying it will result in bonus points.
* Add a UI that tracks the player score and lives left. The player starts with three lives.

### Stretch goal:
* The original game had bunkers that alien bombs and player rockets would slowly destroy.  
  Some console ports had bunkers that would be destroyed after a certain number of hits.  
  Others omitted bunkers entirely, or made different bunkers for different levels.  
  Feel free to add any type of bunker to the game.
* Have fun with particle effects! You aren't restricted to the original hardware, so feel free to add as much game juice (particles, sounds, screen shake) as possible.

{{< expand "Showcase" >}} {{< include file="showcase/invaders.md" type=page >}} {{< /expand >}}
