---
title: "Showcase Footer"
anchor: showcase_footer
---

---

{{< hint type=info title="Want to get your game submission featured here?" >}}

Don't forget to share your progress on the [community discord](https://discord.gg/mBGd9hahZv), or with the hashtag [#20_games_challenge](https://twitter.com/hashtag/20_games_challenge)!  

I'll periodically update this section with community submissions. Feel free to message me if you want yours to be included.
{{< /hint >}}