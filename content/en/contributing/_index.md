---
title: "Feedback"
anchor: "contributing"
weight: 100
---

The [website is open-sourced](https://gitlab.com/20_games_challenge/20_games_challenge.gitlab.io), so any feedback, suggestions, or contributions are appreciated!  
It's made with [Hugo](https://gohugo.io/), and uses the [Geekdocs](https://geekdocs.de/) theme

## Game Details Page
All difficulty assessments and definitions of done are estimates (with the exception of games I've made myself), so feel free to let me know if you disagree with an estimate.

If you have created a game for yourself, please let me know how it went, and if there are any parts of the game page that I should update.

### Balancing
My intention is to keep the 20 Games Challenge consistent. Each game should be a little bit harder than the previous one. If you are following the recommended list on the [challenge]({{< ref "challenge/_index.md" >}}) page, then I'd love to hear how it went.  
* Was any game disproportionately difficult?
* Were there too many "too easy" games in a row?
* Was there any major skill or concept that you didn't get to practice during the challenge?

I'll update the challenge in response to feedback. I'll also store older versions somewhere so you don't have to change plans if you are partway through it when things change.

## Getting featured
Each game page has a showcase section for playable versions, source code, write-ups, tutorials, and devlogs. If finished a game in the challenge and want to be added to the list, then please let me know.  
(Disclaimer: I won't promise that everything will be included as I want to emphasize quality, but I will at least consider adding anything that fits with the challenge.)

### Contact me
I have a personal [Discord](https://discord.gg/ZZ3BP3abRg) if you want to get in touch.  
There is also a community managed Discord [here](https://discord.gg/mBGd9hahZv) if you want to do the challenge with a community.  
There are feedback channels in both servers, so feel free to drop ideas or suggestions in either one.  
I also sometimes remember to check Twitter [@Matt_SDG_Games](https://twitter.com/Matt_SDG_Games).